import * as d3 from 'd3'
import data from './data.json'

const margin = { top: 40, right: 90, bottom: 50, left: 150 }
const width = 900 - margin.left - margin.right
const height = 650 - margin.top - margin.bottom
const separationConstant = 1

const line = d3.line()
  .x(d => width - d.y + margin.left)
  .y(d => d.x + margin.top)
  .curve(d3.curveStep)

const treemap = d3.tree()
  .size([height, width])
  .separation((a, b) => a.parent === b.parent ? 1 : separationConstant)

let nodes = d3.hierarchy(data)
nodes = treemap(nodes)

const svg = d3.select('svg')
  .attr('width', width + margin.left + margin.right)
  .attr('height', height + margin.top + margin.bottom)

const g = svg.append('g')
  .attr('transform', 'translate' + margin.left + ',' + margin.top + ')')

g.selectAll('.link')
  .data(nodes.descendants().slice(1))
  .enter().append('path')
  .attr('class', 'link')
  .attr('d', d => line([d, d.parent]))
  .classed('win', d => d.data.finished)

function gameTemplate (d) {
  return `<div class="row ${(d.data.finished && d.data.scoreA > d.data.scoreB ? 'winner' : '')}">
    <span class="cell name">${d.data.playerA || '&nbsp;'}</span>
    <span class="cell score">${(d.data.scoreA ? d.data.scoreA : '')}</span>
  </div>
  <div class="row ${(d.data.scoreB > d.data.scoreA ? 'winner' : '')}">
    <span class="cell name">${(d.data.playerB || '&nbsp;')}</span>
    <span class="cell score">${(d.data.scoreB ? d.data.scoreB : '')}</span>
  </div>`
}

d3.select('#labels')
  .selectAll('div')
  .data(nodes.descendants())
  .enter()
  .append('div')
  .classed('table', true)
  .classed('played', d => (d.data.scoreA || d.data.scoreB))

  .style('left', d => (width - d.y + margin.left - 100) + 'px')
  .style('top', d => (d.x + (!d.data.playerB ? 12 : 0) + (!d.data.children ? -4 : 0) + 10) + 'px')
  .html(d => gameTemplate(d))
